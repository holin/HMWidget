//
//  HMAppDelegate.h
//  HMWidget
//
//  Created by CocoaPods on 04/02/2015.
//  Copyright (c) 2014 Holin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HMAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
