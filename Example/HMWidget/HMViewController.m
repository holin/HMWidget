//
//  HMViewController.m
//  HMWidget
//
//  Created by Holin on 04/02/2015.
//  Copyright (c) 2014 Holin. All rights reserved.
//

#import "HMViewController.h"
#import "HMWidgetBox.h"

@interface HMViewController ()
@property (weak, nonatomic) IBOutlet HMWidgetBox *box;

@end

@implementation HMViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    NSDictionary* data = @{
                           @"widgets" : @[
                                   @{
                                       @"name" : @"HMWidgetHorizontalOneThree",
                                       @"data" : @{
                                               @"title": @"热门场馆",
                                               @"subtitle": @"热门的场馆",
                                               @"image": @"2.jpg",
                                               @"url": @"http://weixin.1yd.me/2",
                                               
                                               }
                                       },
                                   @{
                                       @"name" : @"HMWidgetHorizontalOneThree",
                                       @"data" : @{
                                               @"title": @"热门场馆",
                                               @"subtitle": @"热门的场馆",
                                               @"image": @"1.jpg",
                                               @"url": @"http://weixin.1yd.me/3",
                                               
                                               }
                                       },
                                   @{
                                       @"name" : @"HMWidgetVerticalFull",
                                       @"data" : @{
                                               @"title": @"热门场馆",
                                               @"subtitle": @"热门的场馆",
                                               @"image": @"1.jpg",
                                               @"url": @"http://weixin.1yd.me/1",
                                               
                                               }
                                       },
                                   @{
                                       @"name" : @"HMWidgetHorizontalOneThree",
                                       @"data" : @{
                                               @"title": @"热门场馆",
                                               @"subtitle": @"热门的场馆",
                                               @"image": @"3.jpg",
                                               @"url": @"http://weixin.1yd.me/4",
                                               
                                               }
                                       },
                                   @{
                                       @"name" : @"HMWidgetHorizontalOneThreeOver",
                                       @"data" : @{
                                               @"title": @"热门场馆",
                                               @"subtitle": @"热门的场馆",
                                               @"image": @"2.jpg",
                                               @"url": @"http://weixin.1yd.me/5",
                                               
                                               }
                                       },
                                   ]
                           };
    
    [self.box layoutIfNeeded];
    
    [self.box feed:data];
    [self.box layout];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
