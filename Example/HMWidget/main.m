//
//  main.m
//  HMWidget
//
//  Created by Holin on 04/02/2015.
//  Copyright (c) 2014 Holin. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "HMAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([HMAppDelegate class]));
    }
}
