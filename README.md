# HMWidget

[![CI Status](http://img.shields.io/travis/Holin/HMWidget.svg?style=flat)](https://travis-ci.org/Holin/HMWidget)
[![Version](https://img.shields.io/cocoapods/v/HMWidget.svg?style=flat)](http://cocoapods.org/pods/HMWidget)
[![License](https://img.shields.io/cocoapods/l/HMWidget.svg?style=flat)](http://cocoapods.org/pods/HMWidget)
[![Platform](https://img.shields.io/cocoapods/p/HMWidget.svg?style=flat)](http://cocoapods.org/pods/HMWidget)

## Usage

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

HMWidget is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "HMWidget"
```

## Author

Holin, holin.he@gmail.com

## License

HMWidget is available under the MIT license. See the LICENSE file for more info.
